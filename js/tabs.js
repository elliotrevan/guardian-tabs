(function (Tabs, undefined) {
    'use strict';

    var xmlhttp,
        secCount = 0,
        pumping = true,
        urlSearch = "http://content.guardianapis.com/search",
        paramKey = "?api-key=9wur7sdh84azzazdt3ye54k4",
        paramGeneral = "&show-fields=webTitle,trailText,webUrl&order-by=newest&page-size=",
        pageSize = 10,
        responceObject,
        data = {},
        //I'm assumeing with the "uk news"  I'm searching for news from the UK production office
        newsContext = {"context" : "news", urlParam : "&section=news&productionOffice=UK", "tableBodyId" : "newsTableBody"},
        travelContext = {"context" : "travel", urlParam : "&section=travel", "tableBodyId" : "travelTableBody"},
        footballContext = {"context" : "football", urlParam : "&section=football", "tableBodyId" : "footballTableBody"},
        contextList = [newsContext, travelContext, footballContext],
        queue = [],
        currentContextItem = newsContext;
    
    
    //TODO code for setting refresh intervals using secCount in tick timer
    //TODO render thumnails from thumbnail imageURL in API, I ran out of time boo!
    //TODO loading indicator
    
    
    var handleError = function (statusCode) {
        alert('Error: ' + statusCode);
        //pumping can continue, the next request might still be good
        pumping = true;
    };
    
    var clearTableForCurrentContext = function () {
        //find table body
        var currentTableBody = document.getElementById(currentContextItem.tableBodyId);
        //remove all children
        while (currentTableBody.hasChildNodes()) {
            currentTableBody.removeChild(currentTableBody.firstChild);
        }
    };
    
    var createTableRow = function (arrayItem) {
        //alert(JSON.stringify(arrayItem));
        //get the important bits of data
        var title = arrayItem.webTitle;
        var trailText = arrayItem.fields.trailText;
        var webUrl = arrayItem.webUrl;
        //create new row
        var newtr = document.createElement('tr');
        var newtd = document.createElement('td');
        var rowString = '<p class="count">' + arrayItem.count + ' </p><a class="article-title" href="' + webUrl + '">' + title + '</a><p class="article-summary"> ' + trailText + '</p>';
        newtd.innerHTML = rowString;
        newtr.appendChild(newtd);
        //add row to table 
        var currentTableBody = document.getElementById(currentContextItem.tableBodyId);
        currentTableBody.appendChild(newtr);
    };
    
    var handleResponse = function (responseText) {
        //turn the response into an object
        responceObject = JSON.parse(responseText);
        //check the responce was OK
        if (responceObject.response.status === "ok") {
            //we only run this if we have some items to work with
            if (responceObject.response.total > 0) {
                //before we add our new items lets clear out our old ones
                clearTableForCurrentContext();
                var results = responceObject.response.results;
                var count = 0;
                results.forEach(function (arrayItem) {
                    count = count + 1;
                    arrayItem.count = count;
                    createTableRow(arrayItem);
                });
            }
            //and we are done, lets enable pumping to process the next item
            pumping = true;
        } else {
            //if server response not ok lets handle this
            handleError("Server response is not OK");
            handleError(responseText);
        }
    };
    
    xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === XMLHttpRequest.DONE) {
            if (xmlhttp.status === 200) {
                handleResponse(xmlhttp.responseText);
            } else {
                handleError(xmlhttp.status);
            }
        }
    };
    
    var loadContentForCurrentContextItem = function () {
        var urlString = urlSearch + paramKey + currentContextItem.urlParam + paramGeneral + pageSize;
        xmlhttp.open("GET", urlString, true);
        xmlhttp.send();
    };
    
    // the pump works through each item in the queue
    var pump = function () {
        // pumping is false when we are waiting for something to finish
        if (!pumping) {
            return;
        }
        // if we have items in our queue then remove the first item in the list and process it
        if (queue.length > 0) {
            pumping = false;
            currentContextItem = queue.shift();
            loadContentForCurrentContextItem();
        }
    };
    
    //this runs every second
    var onTick = function () {
        //secCount = secCount + 1;
        pump();
    };
    
    // take our list of context items and add them to the queue
    var addAllItemsToQueue = function () {
        var i = 0;
        for (i = 0; i < contextList.length; i++) {
            queue.push(contextList[i]);
        }
    };
    
    var init = function () {
        addAllItemsToQueue();
        setInterval(onTick, 1000);
    };
    
    var checkIfDOMReady = function () {
        if (document.readyState === "complete") {
            init();
        } else {
            document.addEventListener("DOMContentLoaded", function (event) {
                init();
            });
        }
    };
    
    checkIfDOMReady();

}(window.Tabs = window.Tabs || {}));