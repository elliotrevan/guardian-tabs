module.exports = function (grunt) {
    'use strict';
    // 1. All configuration goes here 
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        
        //check code quality
        jshint: {
            all: ['Gruntfile.js', 'js/*.js', '!js/xcolor.js']
        },
    
        //concatinate files
        concat: {
            options: {
                sourceMap: true
            },
            js: {
                src:  'js/*.js', // All JS in the libs folder
                dest: 'js/concat.js'
            }
        },
        
        //minify files
        uglify: {
            options: {
                sourceMap: true
            },
            build: {
                src: 'js/concat.js',
                dest: 'js-min/concat.min.js'
            }
        },
        
        
        clean: {
            js: ['js/concat.js', 'js/concat.js.map']
        },
        
        //covert scss to css and concatenate
        sass: {
            options: {
                style: 'compressed',
//                    style: 'expanded',
                spawn: false,
                trace: true
            },
            dist: {
                files: {
                    'css/global.css': 'scss/all.scss'
                }
            }
        },
        
        autoprefixer: {
            single_file: {
                options: {
                    browsers: ['last 2 versions']
                },
                src: 'css/global.css'
            }
        },
           
        watch: {
            options: {
                    //livereload: true
            },
            jschange: {
                files: ['js/*.js'],
                tasks: ['jshint', 'concat', 'uglify', 'clean'],
                options: {
                    spawn: false
                }
            },
            
            scss: {
                files: ['scss/modules/*.scss', 'scss/*.scss' ],
                tasks: ['sass', 'autoprefixer'],
                options: {
                    spawn: false
                }
            }
        }
    });

    // 3. Where we tell Grunt we plan to use the plug-ins.
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-autoprefixer');

    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('default', ['concat', 'uglify', 'clean', 'sass', 'jshint', 'watch']);
};